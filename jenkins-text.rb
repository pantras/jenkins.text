require 'json'
require 'rest-client'
require 'yaml'

class Jenkins
  def initialize
    read_configuration
    @jenkins_server = @config[:default] << '/api/json'
  end
  def list_jobs
    response = RestClient::Request.execute(method: :get, url: @jenkins_server)
    json = JSON.parse(response)
    json['jobs'].map { |job| { name: job['name'], url: job['url'] } }
  end
  def filter_jobs(filters)
    list_jobs.select do |job|
      ARGV.reject { |arg| job[:name] =~ /#{arg}/}.empty?
    end
  end
  def read_configuration
    @config ||= YAML.load_file(config_filename)
  end
  def config_filename
    'jenkins-text.yml'
  end
end

jenkins = Jenkins.new
#jenkins = Jenkins.new('https://build.aspera.us/view/Enterprise%20Server/view/3.8/view/4%20-%20Regression/api/json')
puts jenkins.filter_jobs(ARGV)
